package com.example.asistente

import ai.api.AIListener
import ai.api.android.AIConfiguration
import ai.api.android.AIService
import ai.api.model.AIError
import ai.api.model.AIResponse
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.util.jar.Manifest

class MainActivity : AppCompatActivity(), AIListener, TextToSpeech.OnInitListener {

    override fun onResult(result: AIResponse?) {
        val respuesta = result?.result
        val escuchado = respuesta?.resolvedQuery
        val responder = respuesta?.fulfillment?.speech
        reemplazarTextos(escuchado,responder)
    }

    fun reemplazarTextos(escuchado: String?, respuesta: String?)
    {
        tvEsuchando.text = escuchado
        tvRespuesta.text = respuesta
        hablar(respuesta)
    }

    fun hablar(respuesta: String?)
    {
        leer?.speak(respuesta,TextToSpeech.QUEUE_FLUSH,null,null)
    }

    override fun onListeningStarted() {

    }

    override fun onAudioLevel(level: Float) {

    }

    override fun onError(error: AIError?) {
        val error = "Hubo un error!"
        reemplazarTextos(error,error)
    }

    override fun onListeningCanceled() {

    }

    override fun onListeningFinished() {

    }

    val accessToken = "d35f3f3fc25643ea96696258e6c5add3"
    val REQUEST = 200
    var leer: TextToSpeech? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        leer = TextToSpeech(this, this)
        validar()
        configAsistente()
    }

    fun configAsistente()
    {
        val configuracion = AIConfiguration(accessToken, ai.api.AIConfiguration.SupportedLanguages.Spanish, AIConfiguration.RecognitionEngine.System)

        val service = AIService.getService(this,configuracion)

        service.setListener(this)

        Micro.setOnClickListener{ service.startListening() }
    }

    fun validar()
    {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
        {
            solicitarPermiso()
        }
    }

    fun solicitarPermiso()
    {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.RECORD_AUDIO),REQUEST)
        }
    }

    override fun onInit(status: Int) {

    }


}
